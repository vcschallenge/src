angular.module('controllers', []).
controller('mainController', function($scope, ModalService) {
    var controller = $scope;
    
    controller.sortBy = 'Title';
    
    controller.movies = [{
        title: 'Wonder Woman',
        duration: 141,
        actors: 'Gal Gadot, Chris Pine, Robin Wright',
        director: 'Patty Jenkins',
        releaseDate: new Date("05/25/2017"),
        id: 10000001
    }, {
        title: 'Logan',
        duration: 137,
        actors: 'Hugh Jackman, Patrick Stewart, Dafne Keen',
        director: 'James Mangold',
        releaseDate: new Date("03/03/2017"),
        id: 10000002
    }, {
        title: 'Doctor Strange',
        duration: 115,
        actors: 'Benedict Cumberbatch, Chiwetel Ejiofor, Rachel McAdams',
        director: 'Scott Derrickson',
        releaseDate: new Date("10/20/2016"),
        id: 10000003
    }, {
        title: 'Spider-Man: Homecoming',
        duration: 133,
        actors: 'Tom Holland, Michael Keaton, Robert Downey Jr.',
        director: 'Jon Watts',
        releaseDate: new Date("06/28/2017"),
        id: 10000004
    }, {
        title: 'Pirates of the Caribbean: Dead Men Tell No Tales',
        duration: 129,
        actors: 'Johnny Depp, Geoffrey Rush, Javier Bardem',
        director: 'Joachim Rønning, Espen Sandberg',
        releaseDate: new Date("05/26/2017"),
        id: 10000005
    }, {
        title: 'Deadpool',
        duration: 108,
        actors: 'Ryan Reynolds, Morena Baccarin, T.J. Miller',
        director: 'Tim Miller',
        releaseDate: new Date("02/12/2016"),
        id: 10000006
    }, {
        title: 'Dunkirk',
        duration: 106,
        actors: 'Fionn Whitehead, Barry Keoghan, Mark Rylance',
        director: 'Christopher Nolan',
        releaseDate: new Date("07/21/2017"),
        id: 10000007
    }, {
        title: 'Suicide Squad',
        duration: 123,
        actors: 'Will Smith, Jared Leto, Margot Robbie',
        director: 'David Ayer',
        releaseDate: new Date("08/01/2016"),
        id: 10000008
    }];
    
    controller.allMovies = controller.movies.slice();
    
    controller.getRandomInt = function(min, max) {
        min = Math.ceil(min);
        max = Math.floor(max);

        return Math.floor(Math.random() * (max - min)) + min; //The maximum is exclusive and the minimum is inclusive
    }

    controller.search = function() {
        if (!controller.searchText) {
            controller.movies = controller.allMovies.slice();
            controller.sort(controller.sortBy);
        } else {
            controller.movies = controller.allMovies.filter(function(movie) { 
                return movie.title.toLowerCase().startsWith(controller.searchText.toLowerCase());
            });
        }
    }

    controller.updateMovie = function(update, movie) {

        var updateMovie = new Object({
            title: movie.title,
            duration: movie.duration,
            actors: movie.actors,
            director: movie.director,
            releaseDate: movie.releaseDate,
            id: movie.id
        });
        
        ModalService.showModal({
            templateUrl: "updateMovie.html",
            controller: "updateMovieController",
            inputs: {model: {update: update,
                             movie: updateMovie}}
        }).then(function(modal) {

            modal.element.modal();
            modal.close.then(function(result) {
                if (update) {
                    var index = controller.movies.findIndex((obj => obj.id == result.movie.id));
                    
                    controller.movies.splice(index, 1, result.movie);

                    index = controller.allMovies.findIndex((obj => obj.id == result.movie.id));
                    controller.allMovies.splice(index, 1, result.movie);

                } else {
                    result.movie.id = controller.getRandomInt(1, 1000000);
                    controller.movies.push(result.movie);
                    controller.allMovies.push(result.movie);
                }
            });
        });
    }

    controller.deleteMovie = function(movieId) {
        
        ModalService.showModal({
            templateUrl: "deleteMovie.html",
            controller: "deleteMovieController",
            inputs: {model: {}}
        }).then(function(modal) {

            modal.element.modal();
            modal.close.then(function(result) {
                if (result.result) {
                    var index = controller.movies.findIndex((obj => obj.id == movieId));

                    controller.movies.splice(index, 1);

                    index = controller.allMovies.findIndex((obj => obj.id == movieId));
                    controller.allMovies.splice(index, 1);
                }
            });
        });
    }

    controller.sort = function(value) {
        controller.sortBy = value;
        value = value.toLowerCase();
        
        controller.movies.sort(function(movieA, movieB) {
            return movieA[value] > movieB[value];
        });
    }

    controller.init = function() {

        controller.sort(controller.sortBy);
    }

    controller.init();
}).
controller('updateMovieController', function($scope, model, close) {
    
    $scope.movie;

    if (model.update) {
        $scope.title = "Update movie";
        $scope.movie = model.movie;
    } else {
        $scope.title = "Add a movie";
    }

    $scope.close = function(result) {
        close({result: result, movie: $scope.movie}, 200);
    };
}).
controller('deleteMovieController', function($scope, model, close) {

    $scope.close = function(result) {
        close({result: result}, 200);
    };
});
